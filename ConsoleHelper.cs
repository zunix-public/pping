﻿using System;
using System.Text;

namespace PPing
{
    static class ConsoleHelper
    {
        public static void InitConsole()
        {
            Console.OutputEncoding = Encoding.UTF8;
        }

        public static void WriteToConsole(string logMessage, ConsoleColor color = ConsoleColor.White)
        {
            var previousColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(logMessage);
            Console.ForegroundColor = previousColor;
        }
    }
}
