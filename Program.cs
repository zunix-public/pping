﻿using System;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace PPing
{
    class Program {

        readonly static TimeSpan PortConnexionTimeout = TimeSpan.FromMilliseconds(500);

        static int _port;
        static string _host;

        static void Main(string[] args)
        {
            try
            {
                ConsoleHelper.InitConsole();
                ParseConsoleArgs(args);
                IterateOverHost();
            }
            catch (ArgumentException)
            {
                PrintInvalidArguments();
            }
        }

        static void ParseConsoleArgs(string[] args)
        {
            if (args.Length != 2)
                throw new ArgumentException("Invalid arguments count");

            if (!int.TryParse(args[1], out int parsedPort))
                throw new ArgumentException("Port argument is not a number");

            _host = args[0];
            _port = parsedPort;
        }

        static void IterateOverHost()
        {
            while (true)
            {
                ExecuteHostCheck();
                Thread.Sleep(1000);
            }
        }

        static void ExecuteHostCheck()
        {
            var pingReply = ExecutePing();
            if (pingReply.Status != IPStatus.Success)
            {
                ConsoleHelper.WriteToConsole("Ping request timed out", ConsoleColor.DarkRed);
                return;
            }

            bool isPortOpen = IsPortOpen();
            ConsoleHelper.WriteToConsole($"Ping reply from { _host }:" +
                $" Bytes={ pingReply.Buffer.Length }" +
                $" Time={ pingReply.RoundtripTime }ms" +
                $" TTL={ (pingReply.Options?.Ttl) }" +
                $" Port={_port}" +
                $" PortStatus={ (isPortOpen ? "Open" : "Closed") }",
                isPortOpen ? ConsoleColor.White : ConsoleColor.Gray);
        }

        static PingReply ExecutePing()
        {
            Ping pingSender = new Ping();

            // Create a buffer of 32 bytes of data to be transmitted.
            string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            byte[] buffer = Encoding.ASCII.GetBytes(data);

            // Wait 1 seconds for a reply.
            int timeout = 1000;

            // Set options for transmission:
            // The data can go through 64 gateways or routers
            // before it is destroyed, and the data packet
            // cannot be fragmented.
            PingOptions options = new PingOptions(64, true);

            // Send the request.
            return pingSender.Send(_host, timeout, buffer, options);
        }

        static bool IsPortOpen()
        {
            try
            {
                using (var client = new TcpClient())
                {
                    var result = client.BeginConnect(_host, _port, null, null);
                    var success = result.AsyncWaitHandle.WaitOne(PortConnexionTimeout);
                    client.EndConnect(result);
                    return success;
                }
            }
            catch
            {
                return false;
            }
        }

        static void PrintInvalidArguments()
        {
            ConsoleHelper.WriteToConsole("Invalids arguments.");
            ConsoleHelper.WriteToConsole("");
            ConsoleHelper.WriteToConsole("Usage :");
            ConsoleHelper.WriteToConsole($"# {AppDomain.CurrentDomain.FriendlyName} <IP> <PORT>");
            ConsoleHelper.WriteToConsole($"# {AppDomain.CurrentDomain.FriendlyName} 127.0.0.1 80");
            ConsoleHelper.WriteToConsole($"# {AppDomain.CurrentDomain.FriendlyName} localhost 80");
        }
    }
}
